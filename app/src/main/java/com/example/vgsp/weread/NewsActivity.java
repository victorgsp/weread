package com.example.vgsp.weread;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.vgsp.weread.adapter.NewsAdapter;
import com.example.vgsp.weread.model.Feed;
import com.example.vgsp.weread.model.News;
import com.example.vgsp.weread.repository.NewsRepository;
import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.util.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class NewsActivity extends BaseActivity implements AdapterView.OnItemClickListener, OnLoadNewsComplete {

    private static final String ITEM = "item";
    private static final String TITLE = "title";
    private static final String LINK = "link";
    private static final String DESCRIPTION = "description";
    private static final String MEDIA = "media:content";
    private static final String IMAGE_URL = "url";
    private static final CharSequence IMG_SRC = "img src";
    private static final java.lang.String SPLIT_SYMBOL = ">";
    private static final String URL_IMAGE = "urlImage";

    private Feed feed;
    private ListView newsListView;
    private ArrayList<News> newsList;
    private NewsAdapter mAdapter;
    private ImageLoaderConfiguration imageLoaderConfiguration;
    private AsyncTask<String, Void, List<News>> asyncTask;
    private NewsRepository newsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        newsListView = (ListView) findViewById(R.id.newsList);

        this.feed = (Feed) getIntent().getSerializableExtra(Constants.EXTRA_CLICKED_FEED);

        initImageLoader();
        
        newsRepository = new NewsRepository(this);

        configListView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyncTask != null && !asyncTask.isCancelled()){
            asyncTask.cancel(true);
        }

    }

    private void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnLoading(R.drawable.loading)
                .showImageOnFail(R.drawable.loading).cacheInMemory(true).cacheOnDisk(true).build();
        imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(imageLoaderConfiguration);
    }

    private void configListView() {
        newsList = new ArrayList<News>();
        mAdapter = new NewsAdapter(newsList, this, newsRepository);
        newsListView.setAdapter(mAdapter);

        newsListView.setOnItemClickListener(this);

        asyncTask = new RssAsyncTask(this).execute(
                feed.getUrl());
    }

    private List<News> readXML(InputStream is) {
        List<News> newsList =
                new ArrayList<News>();

        try {
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();

            DocumentBuilder builder =
                    factory.newDocumentBuilder();
            Document xmlDocument = builder.parse(is);

            NodeList posts =
                    xmlDocument.getElementsByTagName(ITEM);

            NodeList urlImageNode =
                    xmlDocument.getElementsByTagName("url");
            String siteImageUrl = null;
            if (urlImageNode.getLength() > 0){
                siteImageUrl = urlImageNode.item(0).getTextContent().replace("\n","").trim();
                siteImageUrl = Util.isValidUrl(siteImageUrl) ? siteImageUrl : null;
            }

            for (int i = 0; i < posts.getLength(); i++) {
                String title = null, description = null,
                        link = null, image = null;

                Node post = posts.item(i);

                NodeList postInfo = post.getChildNodes();

                for (int j = 0; j < postInfo.getLength(); j++) {
                    Node info = postInfo.item(j);

                    if (TITLE.equals(info.getNodeName())) {
                        title = info.getTextContent();

                    } else if (LINK.equals(
                            info.getNodeName())) {
                        link = info.getTextContent();

                    } else if (DESCRIPTION.equals(
                            info.getNodeName())) {
                        description = extractText(info.getTextContent()).trim();
                    } else if (MEDIA.equals(
                            info.getNodeName())) {
                        image = ((Element) info).getAttribute(IMAGE_URL);
                    }else if(URL_IMAGE.equals(
                            info.getNodeName())){
                        image = info.getTextContent();
                    }
                }
                News newsToAdd = new News(title, description, link, image);
                newsToAdd.setFeedId(feed.getId());
                newsToAdd.setDefaultSiteImageUrl(siteImageUrl);
                newsList.add(newsToAdd);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return newsList;
    }

    private String extractText(String textContent) {
        if(textContent.contains(IMG_SRC)){
            String[] array = textContent.split(SPLIT_SYMBOL);
            if (array.length > 0)
                textContent = array[array.length - 1];
        }
        return textContent;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this,WebViewActivity.class);
        intent.putExtra(Constants.EXTRA_NEWS_URL, newsList.get(i).getLink());
        startActivity(intent);
    }

    @Override
    public void onLoadNewsComplete(List<News> newsList) {
        saveDownloadNews(newsList);
        this.newsList.clear();
        this.newsList.addAll(newsRepository.findByFeedId(this.feed.getId()));
        mAdapter.notifyDataSetChanged();
    }

    private void saveDownloadNews(List<News> newsList) {
        for (News news: newsList) {
            if(!newsRepository.exists(news)){
                newsRepository.add(news);
            }
        }
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.news_progress_bar;
    }

    class RssAsyncTask extends
            AsyncTask<String, Void, List<News>> {

        private final OnLoadNewsComplete listener;

        public RssAsyncTask(OnLoadNewsComplete listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Antes de baixaro XML, mostra o dialog
            blockActivity(true);
        }

        @Override
        protected List<News> doInBackground(
                String... params) {

            List<News> list = null;
            HttpURLConnection conection = null;
            InputStream is = null;

            try {
                URL url = new URL(params[0]);
                conection = (HttpURLConnection)
                        url.openConnection();
                conection.connect();

                is = conection.getInputStream();
                list = readXML(is);

            } catch (Throwable t) {
                t.printStackTrace();
            } finally {
                try {
                    if (is != null) is.close();
                    if (conection != null) conection.disconnect();
                } catch (Throwable t) {
                }
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<News> result) {
            super.onPostExecute(result);
            blockActivity(false);
            if(listener != null)
            listener.onLoadNewsComplete(result);

        }


    }
}
