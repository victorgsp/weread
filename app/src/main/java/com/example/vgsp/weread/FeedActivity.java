package com.example.vgsp.weread;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.vgsp.weread.adapter.FeedAdapter;
import com.example.vgsp.weread.model.Feed;
import com.example.vgsp.weread.repository.FeedRepository;
import com.example.vgsp.weread.repository.NewsRepository;
import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.wedeploy.WeDeployConnection;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wedeploy.android.Callback;
import com.wedeploy.android.transport.Response;

import java.util.ArrayList;

public class FeedActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView feedsListView;
    private FeedAdapter mAdapter;
    private ArrayList<Feed> feeds;
    private FeedRepository feedRepository;
    private Gson gson;
    private Feed feedToRemove;
    private NewsRepository newsRepository;
    private String copiedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        feedsListView = (ListView) findViewById(R.id.feedList);
        feedsListView.setOnItemClickListener(this);

        FloatingActionButton actionButton = (FloatingActionButton) findViewById(R.id.fabAddFeed);
        actionButton.setOnClickListener(this);

        feedRepository = new FeedRepository(this);
        newsRepository = new NewsRepository(this);

        registerForContextMenu(feedsListView);

        gson = new Gson();

        loadFeedsOnWedeploy();
        
        copiedText = getIntent().getStringExtra(Constants.EXTRA_COPIED_TEXT);
        if (copiedText != null){
            openCreateFeedActivity();
        }
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.feed_progress_bar;
    }

    private void loadFeedsOnWedeploy() {
        blockActivity(true);
        WeDeployConnection.data().get(Constants.END_POINT_FEEDS).execute(new Callback() {
            @Override
            public void onSuccess(Response response) {
                try {
                    feeds = gson.fromJson(response.getBody(), new TypeToken<ArrayList<Feed>>() {
                    }.getType());
                    saveNewFeeds(feeds);
                    configListView();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    blockActivity(false);
                }
            }

            @Override
            public void onFailure(Exception e) {
                blockActivity(false);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        feedRepository.closeDb();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.feedList) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Feed clickedFeed = feeds.get(info.position);
        switch (item.getItemId()) {
            case R.id.menu_item_remove:
                removeFeed(clickedFeed);
                return true;
            case R.id.menu_item_update:
                Intent intent = new Intent(this, CreateFeedActivity.class);
                intent.putExtra(Constants.EXTRA_CLICKED_FEED, clickedFeed);
                startActivityForResult(intent, 0);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void removeFeed(final Feed feedToRemove) {
        this.feedToRemove = feedToRemove;
        blockActivity(true);
        WeDeployConnection.data().delete(WeDeployConnection.getEndpointWithId(feedToRemove.getId())).execute(new Callback() {
            @Override
            public void onSuccess(Response response) {
                try{
                    feedRepository.remove(FeedActivity.this.feedToRemove.getId());
                    newsRepository.removeByFeedId(FeedActivity.this.feedToRemove.getId());
                    feeds.remove(feedToRemove);
                    mAdapter.notifyDataSetChanged();
                    FeedActivity.this.feedToRemove = null;
                    showSnackBar(feedsListView, getString(R.string.msg_removed_feed_success), Snackbar.LENGTH_SHORT);
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    blockActivity(false);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                blockActivity(false);
            }
        });
    }

    private void saveNewFeeds(ArrayList<Feed> feeds) {
        for (Feed feed : feeds) {
            if (!feedRepository.exists(feed)) {
                feedRepository.add(feed);
            }
        }
    }

    private void configListView() {
        feeds.clear();
        feeds.addAll(feedRepository.findByUserId(WeDeployConnection.getUserId()));
        mAdapter = new FeedAdapter(feeds, this);
        feedsListView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddFeed:
                openCreateFeedActivity();
                break;
        }
    }

    private void openCreateFeedActivity() {
        Intent intent = new Intent(this, CreateFeedActivity.class);
        if(copiedText != null){
            intent.putExtra(Constants.EXTRA_COPIED_TEXT, copiedText);
            getIntent().removeExtra(Constants.EXTRA_COPIED_TEXT);
        }
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Constants.RESULT_CODE_FEED_SAVE_SUCCESS) {
            loadFeedsByDatabase();
            showSnackBar(feedsListView, getString(R.string.msg_saved_feed), Snackbar.LENGTH_LONG);
        }
    }

    private void loadFeedsByDatabase() {
        feeds.clear();
        feeds.addAll(feedRepository.findByUserId(WeDeployConnection.getUserId()));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Feed clickedFeed = feeds.get(i);
        Intent intent = new Intent(FeedActivity.this, NewsActivity.class);
        intent.putExtra(Constants.EXTRA_CLICKED_FEED, clickedFeed);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            WeDeployConnection.auth().signOut().execute(new Callback() {
                @Override
                public void onSuccess(Response response) {
                    WeDeployConnection.removeAuthorization();
                    Log.i("INFO", "signOut realizado com sucesso!");
                }

                @Override
                public void onFailure(Exception e) {
                    Log.e("ERROR", "erro ao realizar o signOut!", e);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
