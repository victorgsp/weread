package com.example.vgsp.weread;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.vgsp.weread.util.Constants;

public class WebViewActivity extends BaseActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        
        webView = (WebView) findViewById(R.id.news_web_view);
        webView.setWebViewClient(new WereadWebViewClient());

        String url = getIntent().getStringExtra(Constants.EXTRA_NEWS_URL);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.news_web_view_progress_bar;
    }

    class WereadWebViewClient extends WebViewClient{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgressBar(true);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            showProgressBar(false);
        }
    }
}
