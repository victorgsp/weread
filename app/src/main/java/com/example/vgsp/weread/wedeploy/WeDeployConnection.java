package com.example.vgsp.weread.wedeploy;

import com.example.vgsp.weread.util.Constants;
import com.wedeploy.android.Callback;
import com.wedeploy.android.WeDeploy;
import com.wedeploy.android.WeDeployAuth;
import com.wedeploy.android.WeDeployData;
import com.wedeploy.android.auth.TokenAuthorization;
import com.wedeploy.android.transport.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Victor Guedes on 16/09/2017.
 */

public class WeDeployConnection {
    private static final String ACCESS_TOKEN = "access_token";
    private static final String ID = "id";

    private static WeDeploy weDeploy;
    private static TokenAuthorization authorization;
    private static String userId;

    public static WeDeploy getWeDeploy() {
        if (weDeploy == null) {
            weDeploy = new WeDeploy.Builder().build();
        }
        return weDeploy;
    }

    public static WeDeployAuth auth() {
        return getWeDeploy().auth(Constants.AUTH_URL).authorization(authorization);
    }

    public static WeDeployData data() {
        return getWeDeploy().data(Constants.DB_URL).authorization(authorization);
    }

    public static String getEndpointWithId(String id){
        return Constants.END_POINT_FEEDS + "\\" + id;
    }

    public static String getUserId() {
        return userId;
    }

    public static void createAuthorization(Response response) {
        JSONObject jsonBody;
        String token = null;
        try {
            jsonBody = new JSONObject(response.getBody());
            token = jsonBody.getString(ACCESS_TOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        authorization = new TokenAuthorization(token);

        auth().getCurrentUser().execute(new Callback() {
            @Override
            public void onSuccess(Response response) {
                JSONObject jsonBody;
                try {
                    jsonBody = new JSONObject(response.getBody());
                    userId = jsonBody.getString(ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void removeAuthorization() {
        authorization = null;
    }
}
