package com.example.vgsp.weread.model;

/**
 * Created by Victor Guedes on 17/09/2017.
 */

public class News {

    private int id;
    private String feedId;
    private String imageUrl;
    private String description;
    private String link;
    private String title;
    private Boolean favorite = false;
    private String defaultSiteImageUrl;

    public News(){

    }

    public News(String title, String description, String link) {
        this();
        this.title = title;
        this.description = description;
        this.link = link;
    }

    public News(String title, String description, String link, String imageUrl) {
        this(title, description, link);
        this.imageUrl = imageUrl;
    }

    public News(String title, String description, String link, String imageUrl, int id) {
        this(title, description, link, imageUrl);
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }


    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public void setDefaultSiteImageUrl(String defaultSiteImageUrl) {
        this.defaultSiteImageUrl = defaultSiteImageUrl;
    }

    public String getDefaultSiteImageUrl(){
        return defaultSiteImageUrl;
    }
}
