package com.example.vgsp.weread;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.wedeploy.WeDeployConnection;
import com.wedeploy.android.Callback;
import com.wedeploy.android.transport.Response;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtMail;
    private EditText edtPassword;
    private Button btnSignup;
    private EditText edtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edtMail = (EditText) findViewById(R.id.edtSignUpMail);
        edtPassword = (EditText) findViewById(R.id.edtSignUpPassword);
        edtName = (EditText) findViewById(R.id.edtSignUpName);
        btnSignup = (Button) findViewById(R.id.btnSignUpCreateAccount);

        btnSignup.setOnClickListener(this);
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.signup_progress_bar;
    }

    @Override
    public void onClick(View view) {
        if(allRequiredFieldsFilled()){
            blockActivity(true);
            WeDeployConnection.auth()
                    .createUser(edtMail.getText().toString(), edtPassword.getText().toString(), edtName.getText().toString())
                    .execute(new Callback() {
                        @Override
                        public void onSuccess(Response response) {
                            setResult(Constants.RESULT_CODE_CREATE_USER_SUCCESS);
                            blockActivity(false);
                            finish();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            showSnackBar(btnSignup, R.string.msg_error_create_account, Snackbar.LENGTH_SHORT);
                            blockActivity(false);
                        }
                    });
        }
    }

    private boolean allRequiredFieldsFilled() {
        return verifyRequiredEditText(edtMail) && verifyRequiredEditText(edtPassword) && verifyRequiredEditText(edtName);
    }
}
