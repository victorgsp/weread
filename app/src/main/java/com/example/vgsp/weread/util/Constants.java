package com.example.vgsp.weread.util;

/**
 * Created by vgsp on 15/09/2017.
 */

public class Constants {
    public static final String AUTH_URL = "https://auth-weread.wedeploy.io";
    public static final String DB_URL = "https://data-weread.wedeploy.io";
    public static final String END_POINT_FEEDS = "feeds";

    public static final String EXTRA_NAME_PASSWORD = "EXTRA_NAME_PASSWORD";
    public static final String EXTRA_NAME_MAIL = "EXTRA_NAME_MAIL";
    public static final String EXTRA_CLICKED_FEED = "EXTRA_CLICKED_FEED";
    public static final String EXTRA_COPIED_TEXT = "EXTRA_COPIED_TEXT";

    public static final int RESULT_CODE_CREATE_USER_SUCCESS =  1;
    public static final int RESULT_CODE_FEED_SAVE_SUCCESS = 2;

    public static final String EXTRA_NEWS_URL = "EXTRA_NEWS_URL";
}
