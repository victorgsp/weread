package com.example.vgsp.weread.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.vgsp.weread.database.WereadContract;
import com.example.vgsp.weread.model.News;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vgsp on 19/09/2017.
 */

public class NewsRepository extends BaseRepository {
    private final Context context;
    WereadContract.NewsEntity tableInformation;

    public NewsRepository(Context context) {
        this.context = context;
    }

    public int add(News news){
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(tableInformation.COLUMN_NAME_TITLE, news.getTitle());
        values.put(tableInformation.COLUMN_NAME_DESCRIPTION, news.getDescription());
        values.put(tableInformation.COLUMN_NAME_IMAGE_URL, news.getImageUrl());
        values.put(tableInformation.COLUMN_NAME_DEFAULT_IMAGE_URL, news.getDefaultSiteImageUrl());
        values.put(tableInformation.COLUMN_NAME_LINK, news.getLink());
        values.put(tableInformation.COLUMN_NAME_FAVORITE, news.getFavorite());
        values.put(tableInformation.COLUMN_NAME_FEED_ID, news.getFeedId());

        // Insert the new row, returning the primary key value of the new row
        return (int) getDb(context).insert(tableInformation.TABLE_NAME, null, values);
    }

    public void update(News news){
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(tableInformation.COLUMN_NAME_TITLE, news.getTitle());
        values.put(tableInformation.COLUMN_NAME_DESCRIPTION, news.getDescription());
        values.put(tableInformation.COLUMN_NAME_IMAGE_URL, news.getImageUrl());
        values.put(tableInformation.COLUMN_NAME_LINK, news.getLink());
        values.put(tableInformation.COLUMN_NAME_FAVORITE, getIntValue(news.getFavorite()));
        values.put(tableInformation.COLUMN_NAME_FEED_ID, news.getFeedId());

        String selection = tableInformation._ID + " == ?";
        String[] selectionArgs = { String.valueOf(news.getId()) };

        getDb(context).update(
                tableInformation.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public List<News> findByFeedId (String feedId){

        List<News> newsList = new ArrayList<>();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                tableInformation._ID,
                tableInformation.COLUMN_NAME_TITLE,
                tableInformation.COLUMN_NAME_DESCRIPTION,
                tableInformation.COLUMN_NAME_IMAGE_URL,
                tableInformation.COLUMN_NAME_DEFAULT_IMAGE_URL,
                tableInformation.COLUMN_NAME_LINK,
                tableInformation.COLUMN_NAME_FAVORITE,
                tableInformation.COLUMN_NAME_FEED_ID,
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = tableInformation.COLUMN_NAME_FEED_ID + " = ?";
        String[] selectionArgs = { String.valueOf(feedId) };

// How you want the results sorted in the resulting Cursor
        String sortOrder =
                tableInformation._ID + " DESC";

        Cursor result = getDb(context).query(
                tableInformation.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder);                                  // The sort order


        if (result.getCount() > 0){
            result.moveToFirst();

            do {
                News news = new News();

                news.setDescription(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_DESCRIPTION)));
                news.setFeedId(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_FEED_ID)));
                news.setImageUrl(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_IMAGE_URL)));
                news.setDefaultSiteImageUrl(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_DEFAULT_IMAGE_URL)));
                news.setLink(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_LINK)));
                news.setFavorite(getBoolean(tableInformation.COLUMN_NAME_FAVORITE, result));
                news.setTitle(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_TITLE)));
                news.setId(result.getInt(result.getColumnIndex(tableInformation._ID)));

                newsList.add(news);

            }while (result.moveToNext());
        }

        result.close();

        return newsList;
    }


    public boolean exists(News news) {
        String Query = "Select * from " + tableInformation.TABLE_NAME + " where " +
                tableInformation.COLUMN_NAME_FEED_ID + " = " + news.getFeedId() + " and " +
                WereadContract.NewsEntity.COLUMN_NAME_LINK + " = '" + news.getLink() + "'";
        Cursor cursor = getDb(context).rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void removeByFeedId(String feedId) {
        String selection = tableInformation.COLUMN_NAME_FEED_ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { feedId };
        // Issue SQL statement.
        getDb(context).delete(tableInformation.TABLE_NAME, selection, selectionArgs);
    }
}
