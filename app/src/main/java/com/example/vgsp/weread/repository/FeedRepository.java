package com.example.vgsp.weread.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.vgsp.weread.database.WereadContract;
import com.example.vgsp.weread.model.Feed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vgsp on 19/09/2017.
 */

public class FeedRepository extends BaseRepository {
    private final Context context;
    WereadContract.FeedEntity tableInformation;

    public FeedRepository(Context context) {
        this.context = context;
    }

    public String add(Feed feed){
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(tableInformation._ID, feed.getId());
        values.put(tableInformation.COLUMN_NAME_TITLE, feed.getTitle());
        values.put(tableInformation.COLUMN_NAME_URL, feed.getUrl());
        values.put(tableInformation.COLUMN_NAME_USER_ID, feed.getUserId());

        // Insert the new row, returning the primary key value of the new row
        return String.valueOf(getDb(context).insert(tableInformation.TABLE_NAME, null, values));
    }

    public void update(Feed feed){
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(tableInformation.COLUMN_NAME_TITLE, feed.getTitle());
        values.put(tableInformation.COLUMN_NAME_URL, feed.getUrl());
        values.put(tableInformation.COLUMN_NAME_USER_ID, feed.getUserId());

        String selection = tableInformation._ID + " == ?";
        String[] selectionArgs = { String.valueOf(feed.getId()) };

        getDb(context).update(
                tableInformation.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public List<Feed> findByUserId (String userId){

        List<Feed> feeds = new ArrayList<>();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                tableInformation._ID,
                tableInformation.COLUMN_NAME_TITLE,
                tableInformation.COLUMN_NAME_URL,
                tableInformation.COLUMN_NAME_USER_ID
        };

        String selection = tableInformation.COLUMN_NAME_USER_ID + " = ?";
        String[] selectionArgs = { String.valueOf(userId) };

// How you want the results sorted in the resulting Cursor
        String sortOrder =
                tableInformation.COLUMN_NAME_TITLE + " ASC";

        Cursor result = getDb(context).query(
                tableInformation.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder);                                  // The sort order


        if (result.getCount() > 0){
            result.moveToFirst();

            do {
                Feed feed = new Feed();

                feed.setTitle(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_TITLE)));
                feed.setId(result.getString(result.getColumnIndex(tableInformation._ID)));
                feed.setUrl(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_URL)));
                feed.setUserId(result.getString(result.getColumnIndex(tableInformation.COLUMN_NAME_USER_ID)));

                feeds.add(feed);

            }while (result.moveToNext());
        }

        return feeds;
    }

    public void remove(String id) {
        String selection = tableInformation._ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { id };
        // Issue SQL statement.
        getDb(context).delete(tableInformation.TABLE_NAME, selection, selectionArgs);
    }

    public boolean exists(Feed feed) {
        String Query = "Select * from " + tableInformation.TABLE_NAME + " where " +
                tableInformation.COLUMN_NAME_URL + " = '" + feed.getUrl() + "' and " +
                tableInformation.COLUMN_NAME_USER_ID + " = " + feed.getUserId();
        Cursor cursor = getDb(context).rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}
