package com.example.vgsp.weread.repository;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vgsp.weread.database.WeReaderDbHelper;

/**
 * Created by vgsp on 19/09/2017.
 */

public class BaseRepository {
    private static WeReaderDbHelper mDbHelper;
    private static SQLiteDatabase db;

    protected static SQLiteDatabase getDb(Context context){
        if ((db == null || !db.isOpen()) && context != null){
            mDbHelper = new WeReaderDbHelper(context.getApplicationContext());
            db = mDbHelper.getWritableDatabase();
        }
        return db;
    }

    public static void closeDb(){
        if (db.isOpen()){
            mDbHelper.close();
            db.close();
        }
    }

    protected boolean getBoolean(String columnName, Cursor result){
        return result.getInt(result.getColumnIndex(columnName)) == 1;
    }

    protected int getIntValue(Boolean favorite) {
        return favorite ? 1 : 0;
    }
}
