package com.example.vgsp.weread.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vgsp.weread.R;
import com.example.vgsp.weread.model.News;
import com.example.vgsp.weread.repository.NewsRepository;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by Victor Guedes on 16/09/2017.
 */

public class NewsAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final Context context;
    private final NewsRepository newsRepository;
    private List<News> newsList;


    public NewsAdapter(List<News> newsList, Context context, NewsRepository newsRepository) {
        this.newsList = newsList;
        this.context = context;
        this.newsRepository = newsRepository;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return newsList.size();
    }

    @Override
    public Object getItem(int i) {
        return newsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater row = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = row.inflate(R.layout.image_new_row, null);

            viewHolder = new ViewHolder();
            viewHolder.mTitle = convertView.findViewById(R.id.txtNewsTitle);
            viewHolder.mImage = convertView.findViewById(R.id.imageNews);
            viewHolder.mDescription = convertView.findViewById(R.id.txtDescription);
            viewHolder.mCheckBox = convertView.findViewById(R.id.checkBoxFavorite);
            viewHolder.mShareBtn = convertView.findViewById(R.id.btnShare);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        News news = newsList.get(position);
        viewHolder.mTitle.setText(news.getTitle());
        viewHolder.mDescription.setText(news.getDescription());
        viewHolder.mCheckBox.setChecked(news.getFavorite());
        viewHolder.mCheckBox.setTag(news);

        viewHolder.mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;

                News news = (News) checkBox.getTag();
                news.setFavorite(checkBox.isChecked());

                newsRepository.update(news);
            }
        });

        viewHolder.mShareBtn.setTag(news);
        viewHolder.mShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                News news = (News)view.getTag();
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                String sAux = news.getLink();
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                context.startActivity(Intent.createChooser(i, "choose one"));
            }
        });

        viewHolder.mImage.setImageBitmap(null);
        if (news.getImageUrl() != null && !news.getImageUrl().isEmpty()){
            ImageLoader.getInstance().displayImage(news.getImageUrl(), viewHolder.mImage);
        }
        else if (news.getDefaultSiteImageUrl() != null){
            ImageLoader.getInstance().displayImage(news.getDefaultSiteImageUrl(), viewHolder.mImage);
        }else{
            viewHolder.mImage.setImageResource(R.drawable.loading);
        }

        return convertView;
    }

    static class ViewHolder {
        private TextView mTitle;
        private TextView mDescription;
        private ImageView mImage;
        private CheckBox mCheckBox;
        private ImageButton mShareBtn;

    }
}


