package com.example.vgsp.weread;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;


/**
 * Created by vgsp on 15/09/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected boolean verifyRequiredEditText(EditText editText) {
        editText.setError(null);
        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(getString(R.string.error_required_field));
            editText.requestFocus();
            return false;
        }
        return true;
    }

    protected void showSnackBar(View view, int text, int length) {
        Snackbar.make(view, text, length).setAction("Action", null).show();
    }

    protected void showSnackBar(View view, String text, int length) {
        Snackbar.make(view, text, length).setAction("Action", null).show();
    }

    protected Integer getProgressBarId() {
        return null;
    }

    public void blockActivity(boolean block) {

        if (block) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            showProgressBar(true);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            showProgressBar(false);
        }
    }

    public void showProgressBar(boolean show) {
        ProgressBar progressBar = (ProgressBar) findViewById(getProgressBarId());
        if (progressBar != null) {
            if (show) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}