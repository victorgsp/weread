package com.example.vgsp.weread.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vgsp.weread.R;
import com.example.vgsp.weread.model.Feed;

import java.util.List;

/**
 * Created by Victor Guedes on 16/09/2017.
 */

public class FeedAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final Context context;
    private List<Feed> feeds;


    public FeedAdapter(List<Feed> feeds, Context context) {
        this.feeds = feeds;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public Object getItem(int i) {
        return feeds.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater row = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = row.inflate(R.layout.feed_row, null);

            viewHolder = new ViewHolder();
            viewHolder.mTxtFeedName = convertView.findViewById(R.id.txtFeedName);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.mTxtFeedName.setText(feeds.get(position).getTitle());

        return convertView;
    }

    static class ViewHolder {
        private TextView mTxtFeedName;

    }
}
