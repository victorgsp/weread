package com.example.vgsp.weread;

import com.example.vgsp.weread.model.News;

import java.util.List;

/**
 * Created by vgsp on 18/09/2017.
 */

interface OnLoadNewsComplete {
    void onLoadNewsComplete(List<News> newsList);
}
