package com.example.vgsp.weread;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.vgsp.weread.model.Feed;
import com.example.vgsp.weread.repository.FeedRepository;
import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.wedeploy.WeDeployConnection;
import com.google.gson.Gson;
import com.wedeploy.android.Callback;
import com.wedeploy.android.transport.Response;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateFeedActivity extends BaseActivity implements View.OnClickListener {

    private FloatingActionButton fab;
    private EditText edtUrl;
    private EditText edtTitle;
    private Gson gson;
    private FeedRepository feedRepository;
    private Feed feedToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_feed);

        edtTitle = (EditText) findViewById(R.id.edtTitle);
        edtUrl = (EditText) findViewById(R.id.edtUrl);

        fab = (FloatingActionButton) findViewById(R.id.fabSaveFeed);
        fab.setOnClickListener(this);

        gson = new Gson();

        feedToUpdate = (Feed) getIntent().getSerializableExtra(Constants.EXTRA_CLICKED_FEED);

        String copiedText = getIntent().getStringExtra(Constants.EXTRA_COPIED_TEXT);

        if (copiedText != null) {
            edtUrl.setText(copiedText);
        } else if (feedToUpdate != null) {
            edtTitle.setText(feedToUpdate.getTitle());
            edtUrl.setText(feedToUpdate.getUrl());
        }

        feedRepository = new FeedRepository(this);
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.create_feed_progress_bar;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabSaveFeed:
                if (feedToUpdate == null) {
                    saveFeed();
                } else {
                    updateFeed();
                }
                break;
        }
    }

    private void updateFeed() {

        if (allRequiredFieldsFilled()) {

            String feedId = feedToUpdate.getId();
            feedToUpdate = new Feed(
                    edtUrl.getText().toString(),
                    WeDeployConnection.getUserId(),
                    edtTitle.getText().toString()
            );

            JSONObject feedToJson = null;
            try {
                feedToJson = new JSONObject(gson.toJson(feedToUpdate, Feed.class));
                feedToJson.remove("id");
                feedToUpdate.setId(feedId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (feedToJson != null) {
                WeDeployConnection.data()
                        .update(WeDeployConnection.getEndpointWithId(feedToUpdate.getId()), feedToJson)
                        .execute(new Callback() {
                            @Override
                            public void onSuccess(Response response) {
                                setResult(Constants.RESULT_CODE_FEED_SAVE_SUCCESS);
                                feedRepository.update(feedToUpdate);
                                finish();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                e.printStackTrace();
                            }
                        });
            }

        }
    }

    private void saveFeed() {
        try {
            if (allRequiredFieldsFilled()) {
                JSONObject jsonObject = new JSONObject(gson.toJson(new Feed(
                        edtUrl.getText().toString(), WeDeployConnection.getUserId(), edtTitle.getText().toString()
                ), Feed.class));

                jsonObject.remove("id");

                blockActivity(true);

                WeDeployConnection.data()
                        .create(Constants.END_POINT_FEEDS, jsonObject)
                        .execute(new Callback() {
                            @Override
                            public void onSuccess(Response response) {
                                setResult(Constants.RESULT_CODE_FEED_SAVE_SUCCESS);
                                try {
                                    Feed feed = gson.fromJson(response.getBody(), Feed.class);
                                    feedRepository.add(feed);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    blockActivity(false);
                                }
                                finish();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                e.printStackTrace();
                                blockActivity(false);
                            }
                        });
            }
        } catch (Exception e) {
            showSnackBar(CreateFeedActivity.this.edtTitle, e.getMessage(), Snackbar.LENGTH_SHORT);
            blockActivity(false);
        }
    }

    private boolean allRequiredFieldsFilled() {
        return verifyRequiredEditText(edtTitle) && verifyRequiredEditText(edtUrl);
    }
}
