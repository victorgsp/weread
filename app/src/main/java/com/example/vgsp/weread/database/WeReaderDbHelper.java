package com.example.vgsp.weread.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vgsp on 19/09/2017.
 */

public class WeReaderDbHelper extends SQLiteOpenHelper {


    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_FEED_TABLE =
            "CREATE TABLE IF NOT EXISTS " + WereadContract.FeedEntity.TABLE_NAME + " (" +
                    WereadContract.FeedEntity._ID + TEXT_TYPE + " PRIMARY KEY NOT NULL," +
                    WereadContract.FeedEntity.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    WereadContract.FeedEntity.COLUMN_NAME_USER_ID + TEXT_TYPE + COMMA_SEP +
                    WereadContract.FeedEntity.COLUMN_NAME_URL + TEXT_TYPE + " )";
    private static final String SQL_CREATE_NEWS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + WereadContract.NewsEntity.TABLE_NAME + " (" +
                    WereadContract.NewsEntity._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    WereadContract.NewsEntity.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_DESCRIPTION  + TEXT_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_IMAGE_URL  + TEXT_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_DEFAULT_IMAGE_URL  + TEXT_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_FEED_ID  + TEXT_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_FAVORITE  + INTEGER_TYPE + COMMA_SEP +
                    WereadContract.NewsEntity.COLUMN_NAME_LINK  + TEXT_TYPE + " )";

    private static final String SQL_DELETE_FEED_TABLE =
            "DROP TABLE IF EXISTS " + WereadContract.FeedEntity.TABLE_NAME;
    private static final String SQL_DELETE_NEWS_TABLE =
            " DROP TABLE IF EXISTS " + WereadContract.NewsEntity.TABLE_NAME;

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Weread.db";

    public WeReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FEED_TABLE);
        db.execSQL(SQL_CREATE_NEWS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_NEWS_TABLE);
        db.execSQL(SQL_DELETE_FEED_TABLE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
