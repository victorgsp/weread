package com.example.vgsp.weread.model;

import java.io.Serializable;

/**
 * Created by Victor Guedes on 16/09/2017.
 */

public class Feed implements Serializable {

    private String title;
    private String userId;
    private String url;
    private String id;

    public Feed(){

    }

    public Feed(String url, String userId, String title){
        this.url = url;
        this.userId = userId;
        this.title = title;
    }

    public Feed(String id, String url, String userId, String title){
        this(url,userId,title);
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
