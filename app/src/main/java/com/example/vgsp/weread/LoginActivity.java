package com.example.vgsp.weread;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.vgsp.weread.service.ClipboardMonitorService;
import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.wedeploy.WeDeployConnection;
import com.example.vgsp.weread.wedeploy.WeDeployExceptionTranslate;
import com.wedeploy.android.Callback;
import com.wedeploy.android.transport.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText edtMail;
    private EditText edtPassword;
    private Button btnSignup;
    private Button btnSignin;
    private String copiedText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtMail = (EditText) findViewById(R.id.edtMail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        btnSignin = (Button) findViewById(R.id.btnSignin);
        btnSignup = (Button) findViewById(R.id.btnSignup);

        btnSignin.setOnClickListener(this);

        btnSignup.setOnClickListener(this);

        startClipboardMonitorService();

        if(getIntent().getStringExtra(Constants.EXTRA_COPIED_TEXT) != null){
            openConfirmDialogToCopiedText();
        }
    }

    private void openConfirmDialogToCopiedText() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.title_create_feed);
        alert.setMessage(R.string.msg_create_copied_feed_confirmation);
        alert.setPositiveButton(R.string.lbl_yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                copiedText = getIntent().getStringExtra(Constants.EXTRA_COPIED_TEXT);
                getIntent().removeExtra(Constants.EXTRA_COPIED_TEXT);
                dialog.dismiss();
            }
        });

        alert.setNegativeButton(R.string.lbl_no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getIntent().removeExtra(Constants.EXTRA_COPIED_TEXT);
                finish();
            }
        });

        alert.show();
    }

    private void startClipboardMonitorService() {
        if(!ClipboardMonitorService.isRunning){
            Log.i("VGSP", "Serviço iniciado com sucesso na tela de login");
            Intent serviceIntent = new Intent(this, ClipboardMonitorService.class);
            startService(serviceIntent);
        }
    }

    @Override
    protected Integer getProgressBarId() {
        return R.id.login_progress_bar;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignin:
                signIn();
                break;
            case R.id.btnSignup:
                signUp();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constants.RESULT_CODE_CREATE_USER_SUCCESS)
            Snackbar.make(btnSignin, R.string.msg_create_user_success, Snackbar.LENGTH_SHORT).setAction("Action", null).show();

    }

    private void signUp() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra(Constants.EXTRA_NAME_MAIL, edtMail.getText().toString());
        intent.putExtra(Constants.EXTRA_NAME_PASSWORD, edtPassword.getText().toString());
        startActivityForResult(intent, 0);
    }

    private void signIn() {
        if (allRequiredFieldsFilled()) {

            blockActivity(true);
            WeDeployConnection.auth().signIn(edtMail.getText().toString(), edtPassword.getText().toString())
                    .execute(new Callback() {
                        @Override
                        public void onSuccess(Response response) {
                            WeDeployConnection.createAuthorization(response);
                            Intent intent = new Intent(LoginActivity.this, FeedActivity.class);
                            if (copiedText != null){
                                intent.putExtra(Constants.EXTRA_COPIED_TEXT, copiedText);
                                getIntent().removeExtra(Constants.EXTRA_COPIED_TEXT);
                            }
                            startActivity(intent);
                            blockActivity(false);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            WeDeployExceptionTranslate translate = new WeDeployExceptionTranslate(e);
                            Snackbar.make(btnSignin, translate.getErrorMessage(), Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                            blockActivity(false);
                        }
                    });
        }
    }

    private boolean allRequiredFieldsFilled() {
        return verifyRequiredEditText(edtMail) && verifyRequiredEditText(edtPassword);
    }
}
