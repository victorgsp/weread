package com.example.vgsp.weread.service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.example.vgsp.weread.LoginActivity;
import com.example.vgsp.weread.util.Constants;
import com.example.vgsp.weread.util.Util;

/**
 * Created by vgsp on 20/09/2017.
 */

public class ClipboardMonitorService extends Service {
    private ClipboardManager clipBoard;
    public static boolean isRunning = false;
    public static String RSS = "rss";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        isRunning = true;
        clipBoard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        clipBoard.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                try {
                    String copiedText = String.valueOf(clipBoard.getPrimaryClip().getItemAt(0).getText());
                    if (Util.isValidUrl(copiedText) && copiedText.contains(RSS)) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.putExtra(Constants.EXTRA_COPIED_TEXT, copiedText);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("VGSP", "Serviço do clipboardmonitor finalizado!");
    }
}
