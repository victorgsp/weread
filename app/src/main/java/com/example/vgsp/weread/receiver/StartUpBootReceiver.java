package com.example.vgsp.weread.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.vgsp.weread.service.ClipboardMonitorService;

/**
 * Created by vgsp on 20/09/2017.
 */

public class StartUpBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            if(!ClipboardMonitorService.isRunning){
                Log.i("VGSP", "Serviço iniciado com sucesso");
                Intent serviceIntent = new Intent(context, ClipboardMonitorService.class);
                context.startService(serviceIntent);
            }
        }
    }

}
