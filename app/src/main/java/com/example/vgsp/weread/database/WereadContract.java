package com.example.vgsp.weread.database;

import android.provider.BaseColumns;

/**
 * Created by vgsp on 19/09/2017.
 */

public final class WereadContract {
    private WereadContract(){}

    /* Inner class that defines the table contents */
    public static class FeedEntity implements BaseColumns {
        public static final String TABLE_NAME = "feed";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_URL = "url";
    }

    public static class NewsEntity implements BaseColumns {
        public static final String TABLE_NAME = "news";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_LINK = "link";
        public static final String COLUMN_NAME_IMAGE_URL = "image_url";
        public static final String COLUMN_NAME_FEED_ID = "feed_id";
        public static final String COLUMN_NAME_FAVORITE = "favorite";
        public static final String COLUMN_NAME_DEFAULT_IMAGE_URL = "default_image_url";
    }
}
